<?php
	
	function calcularValor($idade, $possuiDoenca){
		$valorInicial = 200;
		$adicional= 1.5;
		$adicionalDoenca= 1.3;
	
		$aux = $valorInicial * pow($adicional , $idade);
		
		if ($possuiDoenca== "s"){
			$preco= $aux * $adicionalDoenca;
		}else{
			$preco = $aux;
		}
		return $preco;
	}
	echo "Oi, " .htmlentities($_POST['nome']);
	echo "<br><br>";
	echo "A mensalidade do seu seguro fica R$".number_format(calcularValor($_POST['idade'] , $_POST['doençaPrevia']),2,',', '');
?>