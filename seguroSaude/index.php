<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>seguroSaude</title>
	</head>
	<body>
		<h1>seguroSaude</h1>
		<form action="seguroSaude.php" method="POST">
		<fieldset>
			<legend>Informações pessoais</legend>
			Nome: <br>
			<input type="text" name="nome" required>
			<br><br>
			Informe a faixa etária: <br>
			<select name="idade" required>
				<option name="idade" value="0">20 ou menos</option>
				<option name="idade" value="1">21-30</option>
				<option name="idade" value="2">31-40</option>
				<option name="idade" value="3">41-50</option>
				<option name="idade" value="4">51-65</option>
				<option name="idade" value="5">66 ou mais</option>
			</select>
			<br><br>
			Possui doença prévia? <br>
			<input type="radio" name="doençaPrevia" value="s">Sim<br>
			<input type="radio" name="doençaPrevia" value="n" checked>Não<br>
			<br><br>
			<input type="submit">
		</fieldset>
		</form>
	</body>
</html>