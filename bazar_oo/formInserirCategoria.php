<?php

session_start();
if( !isset( $_SESSION["usuario"] ) )
{
    Header("location: inicio.php");
}

?>


<html>
    <head>
        <meta charset="UTF-8"/>

        <script type="text/javascript">
            function validarFormatoDescricao(){
                var descricao = document.getElementById('descricao').value;
                var primeiraLetra = descricao.substring(0,1);
                if (descricao.length < 6 || !primeiraLetra.match(/[A-Z]/g)){
                    alert("Atenção!\nA descrição deve ter no mínimo 6 caracteres e iniciar com letra maiúscula.\nEx: 'Ferramentas'");
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>

        <?php require_once("cabecalho.inc")?>

        <div>
            <form name="inserirCategoria" action="inserirCategoria.php" method="post" onSubmit="return validarFormatoDescricao()">
                <span>Descrição:</span><input type="text" name="descricao" id="descricao">*
                <br>
                <span>Taxa:</span><input type="text" name="taxa" >*
                <button type="submit">Adicionar</button>
            </form>
        </div>

        <?php require_once("rodape.inc")?>
    </body>

</html>













