<?php
    ob_start();
    require_once( "./comum.php");
    require_once( BASE_DIR . "/classes/Categoria.php");


    $descricao = $_POST["descricao"];
    $taxa = $_POST["taxa"];

    $categoria = new Categoria( null, $descricao, $taxa);

    $resultado = CategoriaDao::add( $categoria);

    if( $resultado ){
        ob_end_clean();
        header("Location: categoria.php?sucesso=Categoria " . urlencode($descricao) . " criada com sucesso." );

    }
    else
    {
        echo "erro ao inserir nova categoria";
    }


