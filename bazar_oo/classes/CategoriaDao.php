<?php
require_once( BASE_DIR . "/classes/Banco.php");

trait CategoriaDao
{
  public static function rowMapper($idCategoria, $descricao, $taxa)
  {
    return new Categoria( $idCategoria, $descricao, $taxa);
  }

  public static function findAll()
  {
      $pdo = Banco::obterConexao();
      $statement = $pdo->prepare("SELECT idCategoria,descricao,taxa FROM Categoria");
      $statement->execute();
      /*
      return $statement->fetchAll( PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
      "Categoria", array( 'xxxx', 'xxx', 'xxx') );
      */

      return $statement->fetchAll( PDO::FETCH_FUNC, "CategoriaDao::rowMapper" );
  }
    public static  function add (Categoria $categoria){
        $pdo = Banco::obterConexao();
        $statemant = $pdo->query("SELECT max(idCategoria) as maior_id FROM Categoria");
        $registro = $statemant->fetch();
        $maior_id = $registro["maior_id"];

        if($maior_id == null){
            $novo_id = 1;
        }else{
            $novo_id = $maior_id + 1;
        }

        $insere = $pdo->prepare("insert into Categoria values(:idCategoria, :descricao, :taxa)");

        /*$auxId = "idCategoria";
        $auxDesc = ":descricao";
        $auxTax = ":taxa";*/
        $getDesc = $categoria->getDescricao();
        $getTax = $categoria->getTaxa();
        $insere->bindParam("idCategoria", $novo_id, PDO::PARAM_INT);
        $insere->bindParam(":descricao", $getDesc, PDO::PARAM_STR);
        $insere->bindParam(":taxa", $getTax, PDO::PARAM_STR);

        return $insere->execute();
    }

    public static function finById($id)
    {
        $pdo = Banco::obterConexao();
        $statement = $pdo->prepare("SELECT idCategoria,descricao,taxa FROM Categoria WHERE $id = :idCategoria ");
        $statement->execute();


        return $statement;


    }

    public static function delete($id){
        $pdo = Banco::obterConexao();
        $statement = $pdo->prepare("DELETE FROM Categoria where id = :idCategoria");
        $statement->bindParam(":idCategoria", $id);
        $statement->execute();

    }
}



















