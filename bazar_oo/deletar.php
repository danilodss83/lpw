<?php
    ob_start();
    require_once( "./comum.php");
    require_once( BASE_DIR . "/classes/Categoria.php");

    $id = $_GET["idCategoria"];

    $deletar = CategoriaDao::delete($id);

if( $deletar ){
    ob_end_clean();
    header("Location: categoria.php?sucesso=Categoria " . urlencode($id) . " deletado com sucesso." );

}
else
{
    echo "erro ao deletar categoria";
}

