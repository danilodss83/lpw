<?php
	session_start();
    $logado = isset($_SESSION['usuario']);
	$hora= date("H");
	if($hora<12){
        $saudar = "Bom dia";
    }else if($hora<18){
        $saudar="Boa tarde";
    }else{
        $saudar = "Boa noite";
    }
?>

<html>
    <head>
        <title></title>
    </head>

    <?php
    require_once("cabec.html");
    ?>
    <br><span><?= $saudar . ', ' . $_SESSION["usuario"] .'! Bem vindo!'?></span>
    <br><br><span>Veja nossos <a href="produtos.php">produtos.</a></span>
    <?php
    require_once("rodape.html");
    ?>

</html>