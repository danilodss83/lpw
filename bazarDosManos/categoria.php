<?php
    session_start();
    if(!isset($_SESSION['usuario'])){
        header("location:index.php");
    }else{
        $logado=($_SESSION['usuario']);
    }

    $pdo=new PDO('mysql:host=localhost;dbname=bazartemtudo;charset=utf8mb4','root','vertrigo');
    $pdo-> setAttribute(PDO :: ATTR_ERRMODE, PDO :: ERRMODE_EXCEPTION );
    $statement= $pdo->query("SELECT idcategoria, nome, descricao FROM categoria");
    $categorias= $statement->fetchAll();
?>

<html>
    <head>
        <meta=charset="UTF-8"/>
        <title>BazarComBanco</title>
    </head>
    <body>
        <?php require_once("cabec.html");?>
        <table border="1">
            <tr>
                <th>Código</th>
                <th>Descrição</th>
            </tr>
                <?php
                    foreach($categorias as $categoria){
                ?>
                        <tr>
                            <td><?= $categoria["idcategoria"] ?></td>
                            <td><?= $categoria["descricao"] ?></td>

                        </tr>
                <?php
                    }
                ?>

        </table>
        <a =href="novaCategoria.php">Nova categoria</a>

        <?php require_once("rodape.html");?>
    </body>


</html>
