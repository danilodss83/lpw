﻿<!DOCTYPE html>

<html lang="pt-br">
	<head>
		<title>Produtos</title>
		<meta charset="UTF-8"/>
		<link rel="stylesheet" type="text/css" href="estilo.css"/>
	</head>
	<body>
		<?php
			require_once("cabec.cab");
		?>
		<div >
			<table>
				<tr>
					<td>
						<figure>
						<img src="brasil.jpg"/>
						<figcaption>
							<p>Camisa Seleção Brasileira por R$249,90</p>
						</figcaption>
						</figure>
					</td>
				</tr>
				<tr>
					<td>
						<figure>
							<img src="flamengo.jpg" />
							<figcaption>
								<p>Camisa Flamengo R$199,90</p>
							</figcaption>
						</figure>
					</td>
				</tr>
				
			</table>
		</div>
		<?php
			require_once("rodape.rod");
		?>
	</body>
		
</html>