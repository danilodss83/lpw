<?php
			
	$hora= date("H");
	if($hora<12){
	$saudar = "Bom dia!";
	}else if($hora<18){
	$saudar="Boa tarde!";
	}else{
	$saudar = "Boa noite!";
	}
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Bazar tem tudo</title>
		<meta charset="UTF-8"/>
		<link rel="stylesheet" type="text/css" href="estilo.css"/>
		
	</head>
	
	<body>
	
		<?php 
			require_once("cabec.cab");
		?>
		
		
		<br>
		
		<span><?=$saudar?>!</span>
		<p >Acesse nosso <a href="produtos.php">catálogo</a> para ver nossos produtos!</p>
		
		<?php
			require_once("rodape.rod");
		?>
	</body>

</html>