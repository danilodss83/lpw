<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8"/>
		<title></title>
		<link rel="stylesheet" type="text/css" href="estilo.css"/>
	</head>
	<body>
		
		<ul>
		<?php
			function numeroParaTexto($x){
					
					if($x % 3==0 && $x % 5 ==0){
								
								echo " <li class='ploc'>$x. Ploc</li>";
							}
					
					elseif($x % 3 == 0){
						echo " <li class='ping'>$x. Ping</li>";
					
					}elseif($x % 5 == 0){
								
								echo " <li class='pong'>$x. Pong</li>";
							}
							
							else{
								echo " <li>$x. Ok</li>";
							}
					
				}
				
				$n=100;
				if( isset($_GET["n"]) && is_numeric($_GET["n"])){ 
					$n=$_GET["n"];
				}
			
				for($x=1; $x<=$n; $x++){
					
					numeroParaTexto($x);
				
				
				}
		
		
		?>
		</ul>
	
	</body>
	

</html>